import Vue from 'vue'
import App from './App.vue'
import store from './store/store/store'
import router from './router'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';

Vue.config.productionTip = false

Vue.use(Element)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
