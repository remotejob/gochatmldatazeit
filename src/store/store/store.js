import 'es6-promise/auto'
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
   
    serverurl: 'https://api.18node.info/v0/mldata/',
    ask: '',
    lastphrases: [],
    auth: false,
    sdataTable: [],
    menuVisible: true
  },

  getters: {
    getAuth: state => {
      return state.auth
    }
  },

  mutations: {

    setSelection(state, selected) {

      state.sdataTable = selected
    },

    setAsk(state, askphrase) {
      state.ask = askphrase
    },
    
    setAuth(state, pass) {

      if (pass === 'sipvip') {
        state.auth = true
      }

    },
    setMenu(state, showbool) {

      state.menuVisible = showbool

    }

  },

  actions: {

    addSelection(context, selected) {

      context.commit('setSelection', selected)

    },
    setAsk(context, askphrase) {

      context.commit('setAsk', askphrase)

    },
    // addLastPhrase(context, phrase) {
    //   context.commit('setLastPhrases', phrase)
    // },
    loginauth(context, pass) {
      context.commit('setAuth', pass)
    },
    showmenu(context, showbool) {

      context.commit("setMenu", showbool)

    }

  }
});