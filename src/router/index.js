import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)


import store from '@/store/store/store'



const routes = [{
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "about" */ '../views/About.vue'),
    meta: {
      requiresAuth: false,
    },

  },
  {
    path: '/Chats',
    name: 'Chats',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "chats" */ '../views/Chats.vue'),
    meta: {
      requiresAuth: true,
    },

  },
  {
    path: '/Chat/:id',
    name: 'Chat',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "chat" */ '../views/Chat.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/Intents',
    name: 'Intents',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "Intents" */ '../views/Intents.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/Templates',
    name: 'Templates',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "chat" */ '../views/Templates.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/Insert',
    name: 'Insert',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "insert" */ '../views/Insert.vue'),
    meta: {
      requiresAuth: true,
    },
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

  if (to.meta.requiresAuth) {
    if (store.getters["getAuth"]) {
      next()
    } else {
      next('/')
    }
  } else {
    next();
  }

})


export default router